use std::error;
use std::fmt::{Display, Formatter, Result};

#[derive(Debug)]
pub enum Error {
    DBus(dbus::Error),
    UnsupportedMethod,
    UnsupportedDevice,
    UnsupportedType,
}

impl From<dbus::Error> for Error {
    fn from(error: dbus::Error) -> Self {
        Error::DBus(error)
    }
}

impl Display for self::Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        use Error::*;
        match self {
            DBus(e) => write!(f, "{}", e),
            UnsupportedMethod => write!(f, "Unsupported Method"),
            UnsupportedDevice => write!(f, "Unsupported Device"),
            UnsupportedType   => write!(f, "Unsupported type")
        }?;

        Ok(())
    }
}

impl error::Error for Error {
}
