// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: EUPL-1.2

use crate::{common, CaptivePortal, GENERIC_CHECK_URL};
use std::borrow::Cow;
use std::collections::HashMap;
use ureq::Agent;
use anyhow::anyhow;

pub(crate) struct HotsplotsProvider;

impl HotsplotsProvider {
    pub fn new() -> HotsplotsProvider {
        HotsplotsProvider
    }
}

impl CaptivePortal for HotsplotsProvider {
    fn can_handle(&self, ssid: &str) -> bool {
        ["BVG Wi-Fi", "WIFI@DB"].contains(&ssid)
    }

    fn login(&self, http_client: &Agent) -> Result<(), anyhow::Error> {
        let initial_response = common::follow_automatic_redirect(http_client)?;

        let initial_resonse_url: url::Url = initial_response.get_url().try_into()?;

        let params = HashMap::<Cow<str>, Cow<str>>::from_iter(initial_resonse_url.query_pairs());

        let response = http_client
            .post("https://hotsplots.de/auth/login.php")
            .send_form(&[
                ("haveTerms", "1"),
                ("termsOK", "1"),
                ("challenge", params.get("challenge").ok_or(anyhow!("non-existant url param challenge"))?),
                ("uamip", params.get("uamip").ok_or(anyhow!("non-existant url param uamip"))?),
                ("uamport", params.get("uamport").ok_or(anyhow!("non-existant url param uamport"))?),
                ("userurl", GENERIC_CHECK_URL),
                ("myLogin", "agb"),
                ("nasid", params.get("nasid").ok_or(anyhow!("non-existant url param nasid"))?),
            ])?;

        let referer = response.get_url().to_string();
        let url = common::scrape_redirect_url(&response.into_string()?)?;

        http_client.get(&url).set("Referer", &referer).call()?;

        Ok(())
    }
}

#[test]
fn test_scraper() -> anyhow::Result<()> {
    let html = r#"
    <!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
    <head>

    <meta name="robots" content="noindex,nofollow">

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/dropdown.js"></script>
    <script type="text/javascript" src="js/toggle.js"></script>
    <script type="text/javascript" src="js/jquery_modal.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <title>Hotspot Login</title>
    <!--<base href="https://www.hotsplots.de" />-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
    <meta http-equiv="refresh" content="0;url=http://192.168.44.1:80/logon?username=agb_accepted&amp;response=cd9951b5c221175b60f8d14001f1a3a7&amp;userurl=ll://dehttp://detectportal.firefox.com/canonical.html" /><center><img style="float" src="/auth/images/loading.gif"></center>
    "#;

    let url = common::scrape_redirect_url(html)?;
    assert_eq!(url, "http://192.168.44.1:80/logon?username=agb_accepted&response=cd9951b5c221175b60f8d14001f1a3a7&userurl=ll://dehttp://detectportal.firefox.com/canonical.html");

    Ok(())
}
