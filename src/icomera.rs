// SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: EUPL-1.2

use ureq::Agent;

use url::Url;

use crate::{CaptivePortal, common};

pub struct IcomeraProvider;

impl IcomeraProvider {
    pub fn new() -> IcomeraProvider {
        IcomeraProvider
    }
}

impl CaptivePortal for IcomeraProvider {
    fn can_handle(&self, ssid: &str) -> bool {
        [
            "FlixTrain Wi-Fi",
            "FlixBus Wi-Fi"
        ].contains(&ssid)
    }

    fn login(&self, http_client: &Agent) -> anyhow::Result<()> {
        let response = common::follow_automatic_redirect(http_client)?;
        let url = Url::parse(response.get_url())?;
        let host = url.host_str();
        assert!(host.is_some());
        let mut activation_url = Url::parse("https://flixtrain.on.icomera.com/services/cna/welcome")?;
        activation_url.set_host(host)?;

        println!("activation url: {}", activation_url);

        let response = http_client
            .post(activation_url.as_ref())
            .send_form(&[("redirect_uri", "https://kde.org")]);

        println!("{:?}", response);

        Ok(())
    }
}
