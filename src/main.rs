// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: EUPL-1.2

use cookie_store::CookieStore;
use vodafone::VodafoneProvider;
use std::{collections::HashSet, fs::File, io::ErrorKind, io::Write, time::Duration};
use ureq::{Agent, AgentBuilder};

use directories::ProjectDirs;

use serde::{Deserialize, Serialize};

mod cdwifi;
mod icomera;
mod hotsplots;
mod noportal;
mod thecloud;
mod thecloud_hospitality;
mod wifi;
mod wifiberlin;
mod wifionice;
mod vodafone;

mod common;
mod notification;

use crate::{
    cdwifi::CDProvider, icomera::IcomeraProvider,
    hotsplots::HotsplotsProvider, noportal::NoPortalProvider, notification::Answer,
    thecloud::TheCloudProvider, thecloud_hospitality::TheCloudHospitalityProvider,
    wifiberlin::FreeWifiBerlinProvider, wifionice::WiFiOnIceProvider,
};

pub static GENERIC_CHECK_URL: &str = "http://networkcheck.kde.org/";
pub static FIREFOX_USER_AGENT: &str =
    "Mozilla/5.0 (X11; Linux x86_64; rv:108.0) Gecko/20100101 Firefox/108.0";

pub trait CaptivePortal {
    fn can_handle(&self, ssid: &str) -> bool;
    fn login(&self, http_client: &Agent) -> Result<(), anyhow::Error>;
}

fn check_connected(http_client: &Agent) -> bool {
    match http_client.get("http://networkcheck.kde.org").call() {
        // Both status code and site content must indicate OK, as some captive portals replace site
        // content
        Ok(result) => result.status_text() == "OK" && result.into_string().unwrap().trim() == "OK",
        Err(_) => false,
    }
}

fn find_connection<T: wifi::WifiManagement>(
    wm: &T,
    http_client: &Agent,
    providers: &[Box<dyn CaptivePortal>],
    config: &mut Config,
) -> anyhow::Result<()> {
    let access_points = wm.reachable_networks()?;

    // If one of these networks is available, we don't do anything.
    // We do this to avoid connecting to a free wifi when a better network is available, but a spurius error is happening in the connection test.
    let exception_network_reachable = access_points
        .iter()
        .any(|ap| config.exception_ssids.contains(ap));
    if exception_network_reachable {
        return Ok(());
    }

    let known_networks = access_points
        .into_iter()
        .filter(|ap| providers.iter().any(|p| p.can_handle(ap)))
        .collect::<Vec<_>>();

    if !known_networks.is_empty() {
        let network = &known_networks[0];
        println!("Found known network {network}");

        if !config.noninteractive {
            if config.disallowed_ssids.contains(network) {
                println!(
                    "Ignoring WiFi network, as it is marked as disallowed in the configuration"
                );
                return Ok(());
            }

            if !config.allowed_ssids.contains(network) {
                match notification::ask_network_verification(network)? {
                    Answer::Accept => {
                        config.allowed_ssids.insert(network.to_string());
                    }
                    Answer::Reject => {
                        config.disallowed_ssids.insert(network.to_string());
                        config.save();
                        return Ok(());
                    }
                    Answer::AcceptAutomatically => {
                        config.noninteractive = true;
                    }
                }
            }

            config.save();
        }

        println!("Connecting to WiFi network…");
        wm.connect(network)?;

        // If we are still logged in from a previous attempt, stop here
        if check_connected(http_client) {
            return Ok(());
        }

        let provider: Option<&Box<dyn CaptivePortal>> = providers
            .iter()
            .find(|&provider| provider.can_handle(network));

        std::thread::sleep(Duration::from_secs(config.captive_portal_delay_secs));

        match provider {
            Some(provider) => provider.login(http_client)?,
            None => eprintln!("I don't know how to log into this network"),
        }
    } else {
        println!("No known networks found :(");
    }

    Ok(())
}

fn noninteractive_default() -> bool {
    false
}

fn captive_portal_delay_secs_default() -> u64 {
    0
}

#[derive(Deserialize, Serialize, Default)]
struct Config {
    #[serde(default)]
    exception_ssids: Vec<String>,
    #[serde(default)]
    allowed_ssids: HashSet<String>,
    #[serde(default)]
    disallowed_ssids: HashSet<String>,
    #[serde(default = "noninteractive_default")]
    noninteractive: bool,
    #[serde(default = "captive_portal_delay_secs_default")]
    captive_portal_delay_secs: u64,
}

impl Config {
    fn save(&self) {
        let location = Config::location();
        let content = toml::to_string_pretty(&self).expect("Failed to serialize config");
        File::create(location)
            .expect("Failed to create config file")
            .write_all(content.as_bytes())
            .expect("write new configuration");
    }

    fn directory() -> String {
        let dirs = ProjectDirs::from("ghsq.ga", "jbb", "freewifid")
            .expect("Failed to determine config directory");
        let config_dir = dirs.config_dir().to_string_lossy().to_string();

        // Make sure the directory exists
        std::fs::create_dir_all(&config_dir).expect("Failed to create config directory");

        config_dir
    }

    fn location() -> String {
        let config_dir = Config::directory();
        format!("{config_dir}/config.toml")
    }
}

fn main() -> anyhow::Result<()> {
    let providers: &[Box<dyn CaptivePortal>] = &[
        Box::from(FreeWifiBerlinProvider::new()),
        Box::from(HotsplotsProvider::new()),
        Box::from(TheCloudProvider::new()),
        Box::from(TheCloudHospitalityProvider::new()),
        Box::from(NoPortalProvider::new()),
        Box::from(WiFiOnIceProvider::new()),
        Box::from(CDProvider::new()),
        Box::from(IcomeraProvider::new()),
        Box::from(VodafoneProvider::new())
    ];

    let config_file_location = Config::location();
    println!("Using config file location {config_file_location}");

    let mut config = match std::fs::read_to_string(config_file_location) {
        Ok(config) => toml::from_str::<Config>(&config).expect("Failed to parse config file"),
        Err(e) => match e {
            _ if e.kind() == ErrorKind::NotFound => Config::default(),
            _ => {
                eprintln!("Error reading config file, using defaults: {e}");
                Config::default()
            }
        },
    };

    // TODO: Are cookies enabled by default?
    let client = AgentBuilder::new()
        .user_agent(FIREFOX_USER_AGENT)
        .cookie_store(CookieStore::default())
        .build();

    #[cfg(target_os = "linux")]
    let dbus_conn = dbus::blocking::Connection::new_system()?;
    #[cfg(target_os = "linux")]
    let wm = wifi::NetworkManagerWifi::new(&dbus_conn)?;

    loop {
        if !check_connected(&client) {
            if let Err(e) = find_connection(&wm, &client, providers, &mut config) {
                eprintln!("Error occured while trying to connect to a network: {e}");
            }

            if check_connected(&client) {
                println!("Connected ✅");
            }
        } else {
            //println!("already connected");
        }
        std::thread::sleep(Duration::from_secs(30));
    }
}
