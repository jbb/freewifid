// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: EUPL-1.2

use crate::CaptivePortal;

use ureq::Agent;

pub(crate) struct NoPortalProvider;

impl NoPortalProvider {
    pub fn new() -> NoPortalProvider {
        NoPortalProvider
    }
}

impl CaptivePortal for NoPortalProvider {
    fn can_handle(&self, ssid: &str) -> bool {
        ssid.contains("freifunk.net")
    }

    fn login(&self, _: &Agent) -> Result<(), anyhow::Error> {
        Ok(())
    }
}
