// SPDX-FileCopyrightText: 2024 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: EUPL-1.2

use crate::CaptivePortal;

use ureq::Agent;

pub(crate) struct VodafoneProvider;

impl VodafoneProvider {
    pub fn new() -> VodafoneProvider {
        VodafoneProvider
    }
}

impl CaptivePortal for VodafoneProvider {
    fn can_handle(&self, ssid: &str) -> bool {
        ["@BayernWLAN"].contains(&ssid)
    }

    fn login(&self, http_client: &Agent) -> Result<(), anyhow::Error> {
        http_client.get("https://hotspot.vodafone.de/api/v4/login?loginProfile=6").call()?;
        Ok(())
    }
}
