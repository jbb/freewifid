// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: EUPL-1.2

use crate::{common, CaptivePortal};
use ureq::Agent;

pub(crate) struct FreeWifiBerlinProvider;

impl FreeWifiBerlinProvider {
    pub fn new() -> FreeWifiBerlinProvider {
        FreeWifiBerlinProvider
    }
}

impl CaptivePortal for FreeWifiBerlinProvider {
    fn can_handle(&self, ssid: &str) -> bool {
        ["_Free_Wifi_Berlin"].contains(&ssid)
    }

    fn login(&self, http_client: &Agent) -> Result<(), anyhow::Error> {
        println!("Receiving cookies from captive portal…");
        common::follow_automatic_redirect(http_client)?;

        println!("Sending cookies to auth url…");
        http_client
            .get("https://n148.network-auth.com/splash/grant?continue_url=https://www.berlin.de/")
            .set("Referer", "https://campaign.sf-ocmp.com/")
            .call()?;

        Ok(())
    }
}
