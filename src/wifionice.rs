// SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: EUPL-1.2

use crate::{common, CaptivePortal};
use ureq::Agent;

use url::Url;

use anyhow::anyhow;

pub struct WiFiOnIceProvider {}

impl WiFiOnIceProvider {
    pub fn new() -> WiFiOnIceProvider {
        WiFiOnIceProvider {}
    }
}

impl CaptivePortal for WiFiOnIceProvider {
    fn can_handle(&self, ssid: &str) -> bool {
        ["WIFIonICE", "metronom free WLAN"].contains(&ssid)
    }

    fn login(&self, http_client: &Agent) -> Result<(), anyhow::Error> {
        let response = common::follow_automatic_redirect(http_client)?;

        let redirect_url = follow_redirects(http_client, response.get_url())?;
        println!("{}", redirect_url);

        if let Some(csrf_token) = http_client.cookie_store().get_any(
            redirect_url.domain().ok_or(anyhow!("No domain in url"))?,
            "/",
            "csrf",
        ) {
            http_client
                .post(&redirect_url.to_string())
                .send_form(&[("login", "true"), ("CSRFToken", csrf_token.value())])?;
        } else {
            eprintln!("wifionice: Did not receive csrf token, this may indicate the wifionice backend is broken");
        }

        Ok(())
    }
}

fn follow_redirects(http_client: &Agent, url: &str) -> anyhow::Result<Url> {
    let mut redirect_url = Url::parse(url)?;
    println!("redirect url: {}", redirect_url.to_string());

    let mut body = String::new();
    let mut response;

    while {
        body.clear();
        response = http_client.get(&redirect_url.to_string()).call()?;
        response.into_reader().read_to_string(&mut body)?;

        common::scrape_redirect_url(&body).is_ok()
    } {
        let url = common::scrape_redirect_url(&body)?;

        if url.starts_with("/") {
            redirect_url.set_path(&url);
        } else {
            redirect_url = Url::parse(&url)?
        }
        println!("Intermediate url: {}", redirect_url.to_string());
    }

    Ok(redirect_url)
}

#[test]
fn test_scraper() -> anyhow::Result<()> {
    let html = r#"
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="refresh" content="0; url=/de/">
        <title>Redirecting</title>
    </head>
    <body>
        <!--<h1>Redirecting</h1>
        If nothing happens, <a href="/de/">continue to the portal</a>-->
        <!--
            We use a HTML meta redirect due to a bug in the captive browser in
            iPhone. A HTTP redirect causes the captive browser to not show up.
            Sorry, W3C.
        -->
    </body>
</html>
    "#;

    let url = &common::scrape_redirect_url(html)?;
    Ok(assert_eq!(url, "/de/"))
}
